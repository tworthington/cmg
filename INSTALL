Unpack the tar file (tar xzf cmg-0.13.0.tgz), run the install script
('install'). 

Once that has finished you will need to install one or more of the
init scripts (or write your own). Four are supplied: a Gentoo one for
each of the smtp and pop servers and RedHat equivilents (smtpd.gentoo,
pop3d.gentoo, smtpd.rh, and pop3d.rh). Whichever ones you are going to
use should be moved into the /etc/init.d/ directory and renamed just
'pop3d' and 'smtpd'. The actual command line options you want to use
(see below) should be added to the init scripts.

Generally speaking, it is possible to use the smtp server on its own
to handle outgoing email only, but there is little point in using just
the pop3 server since it is designed to work with this particular smtp
server.

CONFIGURATION

Configuration files (which are fairly static) are placed into
/etc/mail/smtp and /etc/mail/pop3d (pop3 only has one configuration
file: accounts). More dynamic files such as the current greylist and
the mail queue go in /var/spool/smtpd (emails awaiting collection via
pop3 go in /var/spool/pop3d).

1 edit the file 'aliases' to contain a list of email addresses and the
  pop3 user name that corresponds to that user. One email address/user
  per line with white space between the address and the user name.

  Each user may have multiple email address which s/he receives email
  from and so may appear multiple times on the right hand side. 

  Additionally, an email address may have multiple, comma separated,
  users on the right hand side.

  If an email address appears more than once on the left hand side any
  mail for that address will be delivered to only one user (which is
  indeterminate due to the use of hashes within the system).

  The special user "-" indicates an address which is not vaild.

EXAMPLE:
dilbert@bigcorp.com	dilbert
hr@bigcorp.com		catbert
catbert@bigcorp.com	catbert
wally@bigcorp.com	wally
alice@bigcorp.com	alice
engineers@bigcorp.com	alice,wally,dilbert
ted@bigcorp.com		ted
viagra@bigcorp.com	-
*@bigcorp.com		carol
*@smallcorp.com		ted

  With this aliases file all mail for the small subsiduary company
  goes to Ted, along with his own BigCorp mail. Likewise, mail for the
  human resources department and Catbert both go to Catbert. Mail for
  the senior engineers is duplicated and sent to their own
  accounts. Email to 'viagra@bigcorp.com' is bounced and Carol then
  gets any other BigCorp mail.

  NB if Carol's entry was first then no one else in BigCorp would get
  ANY email since all incoming email would match her wildcard first.

2 edit the file 'accounts' (in /etc/mail/pop3d) to contain the
  passwords of the users which will be picking up email. One per line,
  user name followed by white space followed by the password. The
  passwords may contain spaces (although not at the start, that's
  discarded).

EXAMPLE:
dilbert		sliderule
catbert		666HaHaHaBowBeforeMeMortals.
wally		java:strong,black
alice		fist of death
ted		password


3 add any domains or email addresses that you hate to the file
  'blacklist'. This file DOES take wild cards in the form of * and ?
  which have their usual filename meanings. One ban per line.

EXAMPLE
*.ru
*.hk
*@aol.com
*@yahoo.com
pointy@bigcorp.com


4 add any people or domains you trust to the file 'whitelist'. These will
  not be subject to the blacklist or to greylisting (see below) as
  well as several of the tests for spam.

EXAMPLE
mary12745@aol.com
opera.com
*ebay.co.uk


5 Edit the file '/etc/mail/smtpd/masq' to contain the domain for
  outgoing email. This will be used in the HELO command and in bounce
  notices. You can set the external hostname by using host@masqdomain
  if you don't want to display the machine's internal name in
  greetings and message IDs.

EXAMPLES (Use only one)
tww.cx
mailgate@tww.cx

6 If you have an upstream ISP who provides an smtp relay you can put it
  (and any other relays you have permission to use) into the file
  'smartrelays' with a priority for each (one per line). Priorities
  run from 0 being a very high priority to 999 being very low.
  Basically, the list of possible mailservers is tried in ascending 
  order.

EXAMPLE smartrelays file:
smtp.ntlword.com  100
mail.bt.com	  600

7 If you need to forward email put the current USER name and the
  redirect address into the file /etc/mail/smtp/redirects.

EXAMPLE redirects file:
ted	sacked@hotmail.com
joe	Joe Smith <joes@fsnet.com>

8 If you are going to allow users to send mail through the smtp server
from a remote location (eg, home), then edit the file
/etc/mail/smtp/login.

EXAMPLE login file:
paul	fatcOmptroller!
geoff	TheMasteris@hometoda$


Init.d

If you use Gentoo then put smtpd.gentoo and pop3d.gentoo into
/etc/init.d/ but without the extensions (ie just named 'smtpd' and
'pop3d'). If you use RedHat then do likewise with smtpd.rh and
pop3d.rh.

The servers may be added to runlevels as desired in the normal way
(sym-link or rc-update etc.)


Command Line Arguments (SMTPD only)

The init script for smtpd can be edited to add command line arguments
or they can be added when starting the server by hand:

-a Autoqueue: any outgoing email is queued rather than sent "live".

-i Instant queuing, if any outgoing mail is added to the queue then
   the queue is processed after the email client quits. This implies
   -a so there is no need to use both.

-q <time> Sets the normal time between sweps through the mail
 queue. The syntax is just "xt" where x is an unsigned number and t
 is a single character representing a time period: m=minutes, h=hours,
 d=days, w=weeks. Seconds are represented by no letter or any
 unrecognised letter.

 Eg:  -q 12h  means to process the queue every 12 hours, while 
      -q 45   means to process the queue every 45 seconds.

-d <name> sets the outgoing domainname instead of using the value in
 /etc/mail/smtp/masq.

-b causes emails which do not have the envelope recipient (ie the RCPT
 TO: xxxxx@xxxx.xxx address) somewhere in the TO:, CC:, or even BCC:
 (which rarely exists) headers to be rejected. This generally causes
 BCC'd emails to be rejected since they almost never have a BCC:
 header.

-g <time> uses the same syntax as -q above to define the length of
 time that the server waits between generating a new greylist entry
 and accepting that email.

-w <time> uses the same syntax as -q above to define the length of
 time between the greylist block (see -g) ending and the server
 deleting the entry from the table. In other words, this is the amount
 of time before the server decides that the email was spam and not
 something important. Note that if the email is resent in time then
 the greylist entry is kept for just over a month. ("w" for "window").

-m Don't require a MESSAGE-ID: header.

-D n Debug level: 1=message tracing, 2=errors, 4=informational,
 8=warnings,16=internal debugging info. Add up the ones you want;
 default is 3 (trace+errors) so you'll need to say -D 0 if you want to
 have none at all.

-l Logging mode: like debug but output redirected to /var/log/smtpd

-L List only mode: Only the white, grey, and blacklists (and IP
   spoofing checks) are used to reject or accept email.

-G no greylisting. Still uses white and black list.

-V Check each message for virii using the clamscan program. Only use
   this if you have actually installed ClamAV 
   (http://clamav.elektrapro.com)!

-S Check sender using the Spamhaus DNS-blocklist service.

-I Use SPF id-confirmation if possible.