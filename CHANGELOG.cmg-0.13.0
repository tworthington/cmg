All changes to smtpd unless otherwise specified.  
0.13.0 
Added dynamic blacklisting. Failed attempts to send messages are
scored and saved on a per-ip basis. If an ip's score exceeds 100 it is
banned. The banned list is cleared every 5000 emails or 4 hours,
whichever is shorter. At that point the number of attempted emails
from the top ten ip's is listed to facilitate blocking at the firewall
level.

SPF checking added. A fail of any kind results in the blocking of that
attempt (and the dynamic banning of further attempts).

Various tinkerings with greylisting window times.

Attempted to bring server and children down in an orderly
fashion. This can be slow due to the RFC-mandated timeouts.

Reduced the default maximum number of children from ten to five. On a
heavily spammed site this greatly reduced the number of spam-bots
connecting after being in force for 24 hours and actually decreased
the number of 421's issued.

Added the ability to list trusted machines for which you will
relay. Added this for a particular client who changed their minds
about needing it, consequently this is untested.

Added authentication to allow relaying per user.

Log messages are now prefixed with the process id and child number
(from 1 to maxchildren) of the process issuing it. This makes tracing
a session MUCH easier!



0.12.0
Added a connect timeout as distinct from the helo timeout. helo
timeout defaults to 5min, connect to 1min

Don't check blacklist for outgoing mail.

Various small formatting changes to debug messages.

Permanent fatal reponses (ie 500+) are now bounced instead of being
left in the queue for re-trying.

Messages in the queue are attempted in order of the ones which have
already been tried, then in order of submission.

Our helo message now includes the hostname part of the masquerade.

Bounced messages now say which recipient caused the bounce.

0.11.3
Added timeout for TCP connection to target peer.

0.11.2
Fixed recv function in pop3d for changed definition.

0.11.1
Further improved handling of peer smtpd dying.

Fixed received header duplication in incoming email.

Added Spamhaus XBL list testing to spam block test.

Made default greylisting holdoff time 10minutes (from 59).

Any attempt to use smtpd as an open relay ends session.

Made redirection backlog clearing use relative path.

Improved virus scanning.

Improved caching of redirect data.

Handle internal mail to blocked address.

0.11.0
Corrected location of installed smartrelay file.

Added redirection ability.

Allowed the timeout counter to continue running between getting a
connection to the peer and actually getting the text of its response.

Changed queue spawning strategy.

0.10.0
Changed recv handling to match the change in the definition of Perl.

Made the queue processor a sub-sub process in the hope that a hung 
queue process will now time out and be relaunched.

Added last-gasp mail relay option for when an outgoing email is refused
by brain-dead servers that block all dynamic DNS servers.

0.9.0

Added log message for max job reached.

Fixed crash when peer dies mid-line.

0.8.0

Added timeouts for the whole of the sending process.

Added support for Clam AntiVirus.

Added Spamhaus DNSBL option.

0.7.1

Fixed bug that prevented greylist from being updated.

0.7.0

Timeout is reset on each client input. This might change to just for
NOOPs or just for legal input in a later version. Previously only data
lines reset their timer.

Moved Mime-version header to initial headers section to try to solve
MS Entourage problem.

Maximum time on queue now a real time rather than a number of tries.

Fixed tainted data bug caused by client SMTP server dying during send.

When accepting an email the sender must have an MX record, an A-record
will not do instead. When sending an email an A record is used as a
last resort as per RFC 2821 section 5.

Added debugging/logging levels: trace, error, info, warnings,
debug. Default is tracing plus errors.

Ensure that there is no timeout value pending when queue processing is
started from main process.

Fixed bug in renaming of failed messages.

0.6.0

Multiple equal preference mail servers are picked randomly.

Multiple copies of the same message to a server are batched into one
DATA statement.

Handle multi-line greeting string from peer server (eg, AOL).

Outgoing email is not failed if only some recipients are not valid.

Added code to recognize Verisign's cyberquatting as a invalid
domain/MX.

Allow masqdomain to be of the form host@domain if it is desired to
present a different hostname to the outside world than the machine
uses internally. 

$localhostname is truncated to everything before first dot.

Removed all the (non-documented) "careof" code since it was very weak.

When multiple copies of a message are sent the response code is the
best of the results. Thus a partial success is returned as a 2xx
code. Unsuccessful copies are requeued.

Rewrote injection code (processmessage()). If auto/instant queuing is
on then everything going out is queued; no checks are done during
injection of the new message.

Rewrote bounce code. Bounces now go into the queue rather than being
sent immediately.

Fixed bug that prevented the number of send tries increasing.

The grey list is occasionally cleared of entries which have been
superseded by changes in the black or white lists.

Fail codes of 500+ are not re-tried. Not being able to contact the
target server is a code of 421.

Email addresses are now case-sensitive before the @ and non-
after. Previously the whole address was insensitive. Black and white
lists are now both _totally_ case insensitive.

The special alias "-" is allowed so that a specific address can be
banned while still letting one person use a wild card:
spam@mydomain.com  -
*@mydomain.com	   brian

Greylist is updated to include anyone email is sent to. Works well for
smaller sites where the outgoing server is also the incoming one but
not so well for very big ones (AOL, Yahoo etc) where it is normally
not.

0.5.0 

Added -G flag to turn off greylisting if wanted.

Stupid bug in checkdomain() fixed.

Multi-line responses during sending are now handled.

Log file handle is now hot.

Instant and auto-queueing now work better.

Accounts now case-insensitive.

Outgoing email always has a proper domain attached to the message-id.

Added lock file to queue to prevent a race condition which results in
spurious error messages but no lost mail.

Added angle brackets to outgoing envelopes to keep Yahoo's servers
happy.

Instant queuing extended to calls to processqueue so that the inital
start-up of the program will always process the queue. This also means
that the entire queue will be tried each time a message is sent. This
may not be desirable. On the other hand, it might be!

0.4.2
Fixed -D flag.

0.4.1 

Fixed bug that made greylist entries always expire after $window time.

0.4

Added minimal spam check mode (-L).

Fixed install script: empty accounts file was being created in
/var/spool/pop3d instead of /etc/mail/pop3d.

Very primative logging option. Basically all the debug info (including
outgoing email!) is logged into /var/log/smtpd

smtpd and pop3d: whitelist, blacklist, accounts, and aliases file now
only re-read when they have changed.

pop3d: creates a new spool dir for an account automatically when a
user is added.

HELO/EHLO in transaction mode are equal to 'RSET'.

HELO domains may be different from the sender's domain but must still
exist. If the two do not match the session continues but more detail
is kept in the tracing headers.

If a sender gives an IP address for their HELO then it MUST be correct
or the session will be ended.

The trace back headers need not have any legal domains if there is
only one of them. This probably still isn't good enough.

0.3

Added autoqueue and instantqueue. Autoqueue adds all outgoing messages
to the queue rather than processing them "live". Instantqueue forces
the queue to be processed after anything has been autoqueued.

Decided that 60 hours was too much and reduced days to 24hrs!

Split queue frequency ($holdoff) from greylist block time ($greytime).

Added window expiry.

Emails without the envelope addressee in them are NOT bounced by
default. This can be turned back on using the new -b flag. This is a
stop-gap until a better way of handling BCC's can be found. This is a
real headache as this type of mis-match is VERY typical of spam.

Added -m flag to allow bad messag-id's through. Few genuine emails
have bad message-ids but lots of spam does so I generally don't use
this flag.

Added various flags (see -h for details).

Failure other than blacklisting does not end session.

More not-good-enough documentation.

0.2.1

Changed "Session started" message to debug only.

0.2
Removed echo of outgoing email even when not debugging.

Added network/mask testing for outgoing mail

Added mail groups

Added wildcards in email to user mapping (eg *@mydomain  jim)

Added install script and settled on file layout

Re-wrote the trace-back check. Lots of emails have "localhost" as
their first step and that always failed the "is there an MX record"
test.

Whitelisting skips all header integrity checks.

Whitelisting uses wildcards correctly.